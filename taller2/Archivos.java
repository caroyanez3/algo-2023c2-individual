package aed;

import java.util.Scanner;
import java.io.FileReader;
import java.io.PrintStream;
import java.io.Reader;

class Archivos {
    float[] leerVector(Scanner entrada, int largo) {
       float[] vec = new float[largo];
       for(int i = 0; i < largo; i++){
        vec[i] = entrada.nextFloat();
       }
       return vec;
    }

    float[][] leerMatriz(Scanner entrada, int filas, int columnas) {
        float[][] matriz = new float[filas][columnas];
        for(int i = 0; i < filas; i++){
            for(int j = 0; j < columnas; j++){
                matriz[i][j] = entrada.nextFloat();
            }
        }
        return matriz;
    }

    String espaciosBlancos (int n){
        String var = "";
        for(int i = 0; i < n; i++){
            var += " ";
        }
        return var;
    }
    void imprimirPiramide(PrintStream salida, int alto) {
        String asteriscos = "*";
        String res = "";
        for(int i = 0; i < alto; i++){
            res = espaciosBlancos(alto - 1 - i) + asteriscos + espaciosBlancos(alto - 1 - i);
            asteriscos = "*" + asteriscos + "*";
            salida.println(res);
        }
                
    }
}

