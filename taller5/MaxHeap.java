package aed;

public class MaxHeap {

    private Router[] heap;
    private int tamaño;
    private int capacidad;

    public MaxHeap(int capacidad) {
        this.capacidad = capacidad;
        this.tamaño = 0;
        this.heap = new Router[capacidad];
    }

    public void insertarRouter(Router router) {
        if (tamaño == capacidad) {
            return;
        }
        heap[tamaño] = router;
        tamaño++;

        heapificarArriba();
    }

    public Router extraerMaximo() {
        if (tamaño == 0) {
            return null; 
        }

        Router maxRouter = heap[0];

        heap[0] = heap[tamaño - 1];
        tamaño--;

        heapificarAbajo();

        return maxRouter;
    }

    private void heapificarArriba() {
        int indice = tamaño - 1;

        while (tienePadre(indice) && obtenerPadre(indice).compareTo(heap[indice]) < 0) {
            intercambiar(indice, obtenerIndicePadre(indice));
            indice = obtenerIndicePadre(indice);
        }
    }

    private void heapificarAbajo() {
        int indice = 0;

        while (tieneHijoIzquierdo(indice)) {
            int indiceHijoMayor = obtenerIndiceHijoIzquierdo(indice);

            if (tieneHijoDerecho(indice) && obtenerHijoDerecho(indice).compareTo(obtenerHijoIzquierdo(indice)) > 0) {
                indiceHijoMayor = obtenerIndiceHijoDerecho(indice);
            }

            if (heap[indice].compareTo(heap[indiceHijoMayor]) > 0) {
                break;
            } else {
                intercambiar(indice, indiceHijoMayor);
            }

            indice = indiceHijoMayor;
        }
    }

    private boolean tienePadre(int indice) {
        return indice > 0;
    }

    private int obtenerIndicePadre(int indice) {
        return (indice - 1) / 2;
    }

    private Router obtenerPadre(int indice) {
        return heap[obtenerIndicePadre(indice)];
    }

    private boolean tieneHijoIzquierdo(int indice) {
        return obtenerIndiceHijoIzquierdo(indice) < tamaño;
    }

    private int obtenerIndiceHijoIzquierdo(int indice) {
        return 2 * indice + 1;
    }

    private Router obtenerHijoIzquierdo(int indice) {
        return heap[obtenerIndiceHijoIzquierdo(indice)];
    }

    private boolean tieneHijoDerecho(int indice) {
        return obtenerIndiceHijoDerecho(indice) < tamaño    ;
    }

    private int obtenerIndiceHijoDerecho(int indice) {
        return 2 * indice + 2;
    }

    private Router obtenerHijoDerecho(int indice) {
        return heap[obtenerIndiceHijoDerecho(indice)];
    }

    private void intercambiar(int indice1, int indice2) {
        Router temporal = heap[indice1];
        heap[indice1] = heap[indice2];
        heap[indice2] = temporal;
    }
}