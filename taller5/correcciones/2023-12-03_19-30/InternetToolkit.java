package aed;

public class InternetToolkit {
    public InternetToolkit() {
    }

    private Fragment[] insertionSort(Fragment[] input) {
        int largo = input.length;
        if (largo > 0) {
            int i = 1;
            while (i < largo) {
                int j = i;
                while (j > 0 && input[j - 1].compareTo(input[j]) > 0) {
                    Fragment aux = input[j];
                    input[j] = input[j - 1];
                    input[j - 1] = aux;
                    j--;
                }
                i++;
            }
        }
        return input;
    }
    
    public Fragment[] tcpReorder(Fragment[] fragments) {
        return insertionSort(fragments);
    }


    public Router[] kTopRouters(Router[] routers, int k, int umbral) {
        MaxHeap routerRanking = new MaxHeap(routers.length);
        Router[] kTopRouters = new Router[k];
    
        for (Router router : routers) {
            if (router.getTrafico() > umbral) {
                routerRanking.insertarRouter(router);
            }
        }
    
        for (int i = 0; i < k && i < routers.length; i++) {
            kTopRouters[i] = routerRanking.extraerMaximo();
        }
    
        return kTopRouters;
    }


    public IPv4Address[] sortIPv4(String[] ipv4) {
    
        IPv4Address[] ipv4Ordenadas = new IPv4Address[ipv4.length];
        for (int i = 0; i < ipv4.length; i++) {
            ipv4Ordenadas[i] = new IPv4Address(ipv4[i]);
        }
        int i = 3;
        while (i >= 0) {
            ListaEnlazada<IPv4Address>[] cubo = new ListaEnlazada[256];
            for (int j = 0; j < ipv4.length; j++) {
                IPv4Address ip = ipv4Ordenadas[j];
                if (cubo[ip.getOctet(i)] != null) {
                    cubo[ip.getOctet(i)].agregarAtras(ip);
                } else {
                    cubo[ip.getOctet(i)] = new ListaEnlazada<>();
                    cubo[ip.getOctet(i)].agregarAtras(ip);
                }
            }
            int k = 0;
            for (int j = 0; j < 256; j++) {
                if (cubo[j] != null) {
                    while (cubo[j].longitud() > 0) {
                        ipv4Ordenadas[k] = cubo[j].obtener(0);
                        cubo[j].eliminar(0);
                        k++;
                    }
                }
            }
            i--;
        }
    
        return ipv4Ordenadas;
    }

}