package aed;

import java.util.*;

public class ListaEnlazada<T> implements Secuencia<T> {
    private Nodo primerNodo;
    private Nodo ultimoNodo;
    private int tamaño;

    private class Nodo {
        private T elemento;
        private Nodo nodoAnterior;
        private Nodo nodoSiguiente;

        public Nodo(T elemento) {
            this.elemento = elemento;
            this.nodoAnterior = null;
            this.nodoSiguiente = null;
        }
    }

    public ListaEnlazada() {
        primerNodo = null;
        ultimoNodo = null;
        tamaño = 0;
    }

    public int longitud() {
        return tamaño;
    }

    public void agregarAdelante(T elem) {
        Nodo nuevoNodo = new Nodo(elem);

        if (tamaño == 0) {
            primerNodo = nuevoNodo;
            ultimoNodo = nuevoNodo;
        } else {
            nuevoNodo.nodoSiguiente = primerNodo; 
            primerNodo.nodoAnterior = nuevoNodo; 
            primerNodo = nuevoNodo; 
        }
    
        tamaño += 1;
    }

    public void agregarAtras(T elem) {
        Nodo nuevoNodo = new Nodo(elem);

        if (tamaño == 0) {
            primerNodo = nuevoNodo;
            ultimoNodo = nuevoNodo;
        } else {
            nuevoNodo.nodoAnterior = ultimoNodo; 
            ultimoNodo.nodoSiguiente = nuevoNodo; 
            ultimoNodo = nuevoNodo; 
        }
    
        tamaño += 1;
    }

    public Nodo obtenerNodo(int i){
        if (i >= tamaño || i < 0){throw new IndexOutOfBoundsException("Posición fuera de rango");}

        Nodo nodoActual = primerNodo;

        for (int indice = 0; indice < i; indice += 1) {
            nodoActual = nodoActual.nodoSiguiente;
        }

        return nodoActual;
    }

    public T obtener(int i) {
        if (i >= tamaño || i < 0){throw new IndexOutOfBoundsException("Posición fuera de rango");}
        return obtenerNodo(i).elemento;
    }

    public void eliminar(int i) {
        if (i >= tamaño || i < 0){throw new IndexOutOfBoundsException("Posición fuera de rango");}

        Nodo eliminarNodo = obtenerNodo(i);

        if (tamaño == 1) {
            primerNodo = null;
            ultimoNodo = null;
        } 
        else if (eliminarNodo == primerNodo) {
            primerNodo = primerNodo.nodoSiguiente;
            primerNodo.nodoAnterior = null;
        } 
        else if (eliminarNodo == ultimoNodo) {
            ultimoNodo = ultimoNodo.nodoAnterior;
            ultimoNodo.nodoSiguiente = null;
        } 
        else {
            Nodo nodoAnterior = eliminarNodo.nodoAnterior;
            Nodo nodoSiguiente = eliminarNodo.nodoSiguiente;

            nodoAnterior.nodoSiguiente = nodoSiguiente;
            nodoSiguiente.nodoAnterior = nodoAnterior;
        }
    
        tamaño -= 1;
    }

    public void modificarPosicion(int indice, T elem) {
        if (indice >= tamaño || indice < 0){throw new IndexOutOfBoundsException("Posición fuera de rango");}

        Nodo cambioNodo = obtenerNodo(indice);

        cambioNodo.elemento = elem;
    }

    public ListaEnlazada<T> copiar() {
        ListaEnlazada<T> copia = new ListaEnlazada<T>();

        for (Nodo nodoActual = primerNodo; nodoActual != null; nodoActual = nodoActual.nodoSiguiente) {

            copia.agregarAtras(nodoActual.elemento); 
        }
    
        return copia;
    }

    public ListaEnlazada(ListaEnlazada<T> lista) {
         
        Nodo nodoCopia = null; 
        Nodo nodoAnteriorCopia = null; 

        for(Nodo nodoOriginal = lista.primerNodo; nodoOriginal != null;nodoOriginal = nodoOriginal.nodoSiguiente){

        nodoCopia = new Nodo(nodoOriginal.elemento);

        if (nodoAnteriorCopia != null) {
            nodoAnteriorCopia.nodoSiguiente = nodoCopia;
            nodoCopia.nodoAnterior = nodoAnteriorCopia;
        } else {
            this.primerNodo = nodoCopia;
        }

        nodoAnteriorCopia = nodoCopia;
    }

    this.ultimoNodo = nodoCopia;
    this.tamaño = lista.tamaño;
    }
    
    @Override
    public String toString() {

        if(tamaño == 0){return "[]";}

        String lista = "[";

        for(Nodo nodoActual = primerNodo; nodoActual != null; nodoActual = nodoActual.nodoSiguiente){
            lista += nodoActual.elemento;

            if (nodoActual.nodoSiguiente != null) {
                lista += ", ";
        }}

        lista += "]";

        return lista;
    }

    private class ListaIterador implements Iterador<T> {
        private int posicion; 

        public ListaIterador(ListaEnlazada<T> lista) {
            this.posicion = 0;
        }

        public boolean haySiguiente() {
            return posicion < tamaño;
        }
        
        public boolean hayAnterior() {
            return posicion > 0;
        }
        
        public T siguiente() {
            int i = posicion;
            posicion = posicion + 1;
            return obtener(i);
        }
        
        public T anterior() {
            posicion = posicion - 1;
            int i = posicion;
            return obtener(i);
        }
    }

    public Iterador<T> iterador() {
        return new ListaIterador(this);
    }

}
