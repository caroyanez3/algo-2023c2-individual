package aed;

import java.util.*;

// Todos los tipos de datos "Comparables" tienen el método compareTo()
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    private Nodo _raiz;
    private int cardinal;
    private Nodo ultimoNodoBuscado;

    private class Nodo {
        T valor;
        Nodo izq;
        Nodo der;
        Nodo padre;

        Nodo(T v) {
            valor = v;
            izq = null;
            der = null;
            padre = null;
        }
    }

    public ABB() {
        _raiz = null;
        cardinal = 0;
        ultimoNodoBuscado = null;
    }

    public int cardinal() {
        return cardinal;
    }

    public T minimo(){
        if(_raiz == null){
            return null;
        }
        Nodo nodoActual = _raiz;

        while (nodoActual.izq != null) {
        nodoActual = nodoActual.izq;
        }
    
        return nodoActual.valor;
    }

    public T maximo(){
        if(_raiz == null){
            return null;
        };
        Nodo nodoActual = _raiz;

        while (nodoActual.der != null) {
        nodoActual = nodoActual.der;
        }
    
        return nodoActual.valor;
    }

    public boolean busquedaRecursiva(Nodo _raiz, T elem) {
        if (_raiz == null) {
            return false;
        }
        
        ultimoNodoBuscado = _raiz;

        int comparacion = elem.compareTo(_raiz.valor);

        if (comparacion < 0) {
            return busquedaRecursiva(_raiz.izq, elem); 
        }
        else if (comparacion > 0) {
            return busquedaRecursiva(_raiz.der, elem);
        } 
        else {
            return true; 
        }
    }

    public boolean pertenece(T elem){
        if (_raiz == null) {
            return false;
        }
        
        ultimoNodoBuscado = _raiz;

        int comparacion = elem.compareTo(_raiz.valor);

        if (comparacion < 0) {
            return busquedaRecursiva(_raiz.izq, elem); 
        }
        else if (comparacion > 0) {
            return busquedaRecursiva(_raiz.der, elem);
        } 
        else {
            return true; 
        }
    }

    public void insertar(T elem){
        if (!pertenece(elem)){
            Nodo nuevoNodo = new Nodo(elem);

            if (_raiz == null) {
                _raiz = nuevoNodo;
                ultimoNodoBuscado = _raiz;
            }

            int comparacion = elem.compareTo(ultimoNodoBuscado.valor);

            if (comparacion < 0) {
                ultimoNodoBuscado.izq = nuevoNodo;
            }
            else if (comparacion > 0) {
                ultimoNodoBuscado.der = nuevoNodo;
            } 
            cardinal += 1;
        }
        
    }
    
    public Nodo inmediatoSucesor(Nodo nodo){
        Nodo nodoSucesor = nodo.der;
        while (nodoSucesor != null && nodoSucesor.izq != null){
            nodoSucesor = nodoSucesor.izq;
        }
        return nodoSucesor;
        }
    
    public void eliminar(T elem) {
        if (pertenece(elem)) {
            _raiz = eliminarRecursivo(_raiz, elem);
            cardinal -= 1;
        }
    }
        
    private Nodo eliminarRecursivo(Nodo nodo, T elem) {
        if (nodo == null) {
            return nodo; 
        }
        
        int comparacion = elem.compareTo(nodo.valor);
        
        if (comparacion < 0) {
            nodo.izq = eliminarRecursivo(nodo.izq, elem);
        } 
        else if (comparacion > 0) {
            nodo.der = eliminarRecursivo(nodo.der, elem);
        } 
        else {
            if (nodo.izq == null) {
                return nodo.der;
            } 
            else if (nodo.der == null) {
                return nodo.izq;
            } 
            else {
                Nodo sucesor = inmediatoSucesor(nodo);
                nodo.valor = sucesor.valor;
                nodo.der = eliminarRecursivo(nodo.der, sucesor.valor);
            }
        }
        return nodo;
    }

    public String toString() {
        List<T> elementosUnicos = new ArrayList<>();

        enOrden(_raiz, elementosUnicos);
        
        if (elementosUnicos.isEmpty()) {
            return "{}";
        }
        
        String lista = "{";
        
        for (int i = 0; i < elementosUnicos.size(); i += 1) {
            lista += elementosUnicos.get(i);
        
            if (i < elementosUnicos.size() - 1) {
                lista += ",";
            }
        }
        
        lista += "}";
        
        return lista;
    }
        
        private void enOrden(Nodo nodoActual, List<T> elementos) {
            if (nodoActual != null) {
                enOrden(nodoActual.izq, elementos);
                if (!elementos.contains(nodoActual.valor)) {
                    elementos.add(nodoActual.valor);
                }
                enOrden(nodoActual.der, elementos);
            }
        }

        public Nodo mayor(){
            Nodo mayor = _raiz;
            while (mayor.der != null){
                mayor = mayor.der;
            }
            return mayor;
            }
    
            private class ABB_Iterador implements Iterador<T> {
                private Nodo _actual;
                private Stack<Nodo> pila;
            
                public ABB_Iterador() {
                    _actual = _raiz;
                    pila = new Stack<>();
                    while (_actual != null) {
                        pila.push(_actual);
                        _actual = _actual.izq;
                    }
                }
            
                public boolean haySiguiente() {
                    return !pila.isEmpty();
                }
            
                public T siguiente() {
                    if (!haySiguiente()) {
                        return null;
                    }
            
                    Nodo nodo = pila.pop();
                    _actual = nodo.der;
            
                    while (_actual != null) {
                        pila.push(_actual);
                        _actual = _actual.izq;
                    }
            
                    return nodo.valor;
                }
            }
            
            public Iterador<T> iterador() {
                return new ABB_Iterador();
            }
    }
