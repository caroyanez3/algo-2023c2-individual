package aed;

class Funciones {
    int cuadrado(int x) {
        return x * x;
    }

    double distancia(double x, double y) {
        return Math.sqrt(x * x + y * y);
    }

    boolean esPar(int n) {
        return n % 2 == 0;
    }

    boolean esBisiesto(int n) {
        return (n % 4 == 0 && n % 100 != 0) || (n % 400 == 0);
    }

    int factorialIterativo(int n) {
        int res = 1;
        for (int i = 1; i <=  n; i++) {
            res = i * res;
        }
        return res;
    }

    int factorialRecursivo(int n) {
        if (n == 0) {return 1;}
        else {return n * factorialRecursivo(n - 1);}   
    } 

    boolean esPrimo(int n) {
        boolean res = true;
        if (n == 0 || n == 1) {
            return false;
        }
        for (int i = 2; i < (n - 1); i++) {
            if (n % i == 0) {
                res = false;
            }
        }
        return res;
    }

    int sumatoria(int[] numeros) {
        int res = 0;
        for (int i = 0; i < numeros.length; i++) {
            res += numeros[i];
        }
        return res;
    }

    int busqueda(int[] numeros, int buscado) {
        int res = -1;
        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] == buscado) {
                res = i;
            }
        }
        return res;
    }

    boolean tienePrimo(int[] numeros) {
        boolean res = false;
        for (int i = 0; i < numeros.length; i++) {
            if (esPrimo(numeros[i])) {
                res = true;
            }
        }
        return res;
    }

    boolean todosPares(int[] numeros) {
        boolean res = true;
        for (int i = 0; i < numeros.length; i++) {
            if (!esPar(numeros[i])) {
                res = false;
            }
        }
        return res;
    }

    boolean esPrefijo(String s1, String s2) {
        boolean res = true;
        if (s1.length() > s2.length()) {
            return false;
        }
        for (int i = 0; i < s1.length(); i++) {
            if (s1.charAt(i) != s2.charAt(i)) {
                res = false; 
            }
        }
        return res;
    }

     boolean esSufijo(String s1, String s2) {
        if (s1.length() > s2.length()) {
            return false;
        }
        int j = s2.length() - 1;
        for (int i = s1.length() - 1; i >= 0; i -= 1){
            if(s1.charAt(i) != s2.charAt(j)){
                return false;
            }
            j -= 1;
        }
        return true;
    } 
}
